# Contributing to Here INFO

Contributions are always welcome!

## Feature request

[Ask](https://gitlab.com/guide42/hereinfo/-/issues/new) whatever you want this
application to have. Let's discuss it.

## Criteria for new views

- Must be of interest to the amateur astronomer and general curious person.
- Should do calculations but do try not use Internet connection.
- Add information about the subject, a few paragraph for the beginner.
- Use SVG when possible to display graphics. Images are OK if they stay small
  and in license.
- Include it in the _correct_ section and folder; or create new ones.
- There is no problem in adding new libraries to use. Check if cannot be done
  with the current ones.
- Better ask for feedback on your idea first.

## Update existing code

Nothing is perfect. Do you see a problem? Send your merge request for a bug fix
or best practices or test cases.

## Without ideas?

Go to [issues](https://gitlab.com/guide42/hereinfo/-/issues) and check in on a
task you can do.
