### Latest Version

### 0.5.0 (2020-10-24)

 - Add solstices and equinoxes to Calendar events.
 - Add Moon max declination events.
 - Re-order sections and views.

### 0.4.0 (2020-09-22)

 - Calendar with chronological events.
 - Add reminder N minutes before event.
 - NEW: Equation of Time.
 - NEW: Horizontal Sundial.

### 0.3.0 (2020-09-11)

 - Remove buttons. Not needed for keyboard navigation.
 - Save geo position and load from history.

### 0.2.0 (2020-08-23)

 - Show and set time of the day (hours and minutes).
 - NEW: Sun Position.
 - NEW: Moon Distance.
 - NEW: Close Solar Eclipse.
 - NEW: Close Lunar Eclipse.
 - NEW: Moon Apsis: Perigee and Apogee.
 - NEW: Earth Apsis: Perihelion and Aphelion.
 - NEW: Moon Node: Ascending and Descending.
 - NEW: Moon Phase Date: New and Full Moon.

### 0.1.0 (2020-08-11)

First public version.
