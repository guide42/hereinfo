import React, { useState } from 'react';
import TabsMenu from './ui/TabsMenu';
import ListSection from './ui/ListSection';
import CalendarEvents from './ui/CalendarEvents';
import SectionSwicher from './ui/SectionSwicher';
import MoreText from './view/MoreText';
import DateTimeAndTimezone from './view/DateTimeAndTimezone';
import GeoLocation from './view/GeoLocation';
import GeoPositionHistory from './view/GeoPositionHistory';
import SunTime from './view/sun/SunTime';
import MoonTime from './view/moon/MoonTime';
import MoonPhase from './view/moon/MoonPhase';
import MoonPhaseDate from './view/moon/MoonPhaseDate';
import MoonDistance from './view/moon/MoonDistance';
import MoonApsis from './view/moon/MoonApsis';
import MoonNode from './view/moon/MoonNode';
import LunarEclipse from './view/moon/LunarEclipse';
import SunPosition from './view/sun/SunPosition';
import EarthApsis from './view/sun/EarthApsis';
import SolarEclipse from './view/sun/SolarEclipse';
import EqTime from './view/sun/EqTime';
import Sundial from './view/sun/Sundial';
import './App.css';

function App() {
  const [date, setDate] = useState(new Date());
  const [geoPosition, setGeoPosition] = useState({
    latitude: null,
    longitude: null,
    altitude: null,
  });
  const [active, setActive] = useState(0);
  const handleMenuChange = newIndex => {
    setActive(newIndex);
  };
  const handleDateChange = newDate => {
    setDate(newDate);
  };
  const handleGeoPositionChange = newGeoPosition => {
    setGeoPosition(newGeoPosition);
  };
  const handleSectionFocus = (sectionIndex, listIndex) => {
    if (sectionIndex !== active) {
      setActive(sectionIndex);
    }
  };
  const sections = [
    <ListSection menuLabel="Here INFO">
      <MoreText>
        <h2>Here INFO</h2>
        <p>
          About where you are here and now, rise and set hours, astronomical
          positions, moon age, and much more.
        </p>
        <h3>Usage</h3>
        <p>
          <label>
            <span className="cq">&larr;</span>
            <span className="cq">&rarr;</span>
          </label>
          Change menu tab.
        </p>
        <p>
          <label>
            <span className="cq">&uarr;</span>
            <span className="cq">&darr;</span>
          </label>
          Change section list item and scroll.
        </p>
        <p>
          <label>
            <span className="cq">Enter</span>
          </label>
          Activate current item.
        </p>
        <p>
          <label>
            <span className="cq">Back</span>
          </label>
          Deactivate current item.
        </p>
        <p>
          <label>
            <span className="cq">Soft Left</span>
          </label>
          Previous form control.
        </p>
        <p>
          <label>
            <span className="cq">Soft Right</span>
          </label>
          Next form control.
        </p>
        <h3>Credits</h3>
        <p>
          Developed by Juan M
          {" "}
          <small>(&laquo;<span style={{color: "blue"}}>jm.guide42.com</span>&raquo;.)</small>
        </p>
        <p>
          Some texts from Wikipedia
          {" "}
          <small>(CC Attribution-ShareAlike.)</small>
        </p>
        <p>
          Telescope icon from Octicons by GitHub Inc
          {" "}
          <small>(MIT License.)</small>
        </p>
        <p>
          Thanks to Banana Hackers.
        </p>
      </MoreText>
      <DateTimeAndTimezone
        date={date}
        onDateChange={handleDateChange}
      />
      <GeoLocation
        latitude={geoPosition.latitude}
        longitude={geoPosition.longitude}
        altitude={geoPosition.altitude}
        onPositionChange={handleGeoPositionChange}
      />
      <GeoPositionHistory
        date={date}
        latitude={geoPosition.latitude}
        longitude={geoPosition.longitude}
        altitude={geoPosition.altitude}
        onPositionChange={handleGeoPositionChange}
      />
    </ListSection>,
    <ListSection menuLabel="Today">
      <SunTime geoPosition={geoPosition} date={date} name="nauticalDawn">
        <p>
          The moment when the geometric center of the Sun is 12 degrees below
          the horizon in the morning.
        </p>
      </SunTime>
      <SunTime geoPosition={geoPosition} date={date} name="dawn">
        <p>
          The time that marks the beginning of twilight before sunrise. It is
          recognized by the appearance of indirect sunlight being scattered in
          Earth's atmosphere, when the centre of the Sun's disc has reached 18°
          below the observer's horizon.
        </p>
      </SunTime>
      <SunTime geoPosition={geoPosition} date={date} name="sunrise">
        <p>
          The moment when the upper limb of the Sun appears on the horizon in
          the morning.
        </p>
        <p>
          At the horizon, the average amount of refraction is 34 arcminutes,
          though this amount varies based on atmospheric conditions.
          The apparent radius of the Sun at the horizon is 16 arcminutes.
          These two angles combine to define sunrise to occur when the Sun's
          center is 50 arcminutes below the horizon, or 90.83° from the zenith.
        </p>
      </SunTime>
      <SunTime geoPosition={geoPosition} date={date} name="solarNoon">
        <p>
          Solar noon is the time when the Sun appears to contact the local
          celestial meridian. This is when the Sun apparently reaches its
          highest point in the sky.
        </p>
      </SunTime>
      <SunTime geoPosition={geoPosition} date={date} name="sunset">
        <p>
          The daily disappearance of the Sun below the horizon due to Earth's
          rotation.
        </p>
        <p>
          Sunset colors are typically more brilliant than sunrise colors,
          because the evening air contains more particles than morning air.
        </p>
        <p>
          Some of the most varied colors at sunset can be found in the opposite
          or eastern sky after the Sun has set during twilight. Depending on
          weather conditions and the types of clouds present, these colors have
          a wide spectrum, and can produce unusual results.
        </p>
      </SunTime>
      <SunTime geoPosition={geoPosition} date={date} name="dusk">
        <p>The darkest part of twilight before night begins.</p>
      </SunTime>
      <SunTime geoPosition={geoPosition} date={date} name="nauticalDusk">
        <p>
          The moment when the geometric center of the Sun is 12 degrees below
          the horizon in the evening.
        </p>
      </SunTime>
      <SunTime geoPosition={geoPosition} date={date} name="night">
        <p>
          The period of ambient darkness from sunset to sunrise during each
          24-hour day, when the Sun is below the horizon.
        </p>
      </SunTime>
      <SunTime geoPosition={geoPosition} date={date} name="nadir">
        <p>
          The darkest moment of the night, when the Sun is in the lowest
          position.
        </p>
      </SunTime>
      <MoonTime geoPosition={geoPosition} date={date}>
        <p>
          The primary lunar surface features detectable to the naked eye are
          the lunar maria or "seas", large basaltic plains which form imaginary
          figures as the traditional "Moon Rabbit". The maria cover about 35%
          of the surface.
        </p>
        <p>
          Earthshine is the faint glow of the non-illuminated (night) side of
          the Moon caused by sunlight reflecting off the surface of Earth and
          onto the night side of the Moon.
        </p>
      </MoonTime>
      <SunTime geoPosition={geoPosition} date={date} name="goldenHour">
        <p>
          The period of time shortly before sunrise and shortly after sunset.
        </p>
        <p>
          During this time the brightness of the sky matches the brightness of
          streetlights, signs, car headlights and lit windows. Also, during
          this period of time there are no sharp shadows because the sun has
          set (or hasn't risen).
        </p>
      </SunTime>
    </ListSection>,
    <CalendarEvents menuLabel="Calendar" date={date} />,
    <ListSection menuLabel="Moon">
      <MoonPhase geoPosition={geoPosition} date={date}>
        <p>
          During the first two weeks, the Moon is called <em>crescent</em>{" "}
          (when the illuminated portion increases) while it is <em>falling</em>{" "}
          for the next two weeks. For two weeks, the crescent Moon wanes before
          and waxes after new moon. The Moon when other than crescent or dark,
          is called a gibbous, waxing before and waning after full moon.
        </p>
        <p>
          The descriptor waxing is used for an intermediate phase when the
          Moon's apparent shape is thickening, from new to full moon, and
          waning when the shape is thinning.
        </p>
      </MoonPhase>
      <MoonPhaseDate date={date}>
        <p>
          There are four principal lunar phases: new moon, first quarter,
          full moon, and last quarter (also known as third or final quarter),
          when the Moon's ecliptic longitude is at an angle to the Sun (as
          viewed from Earth) of 0°, 90°, 180°, and 270°, respectively.
        </p>
        <p>
          The longest duration between full moon to new moon (or new moon to
          full moon) lasts about 15 days and 14.5 hours, while the shortest
          duration between full moon to new moon (or new moon to full moon)
          lasts only about 13 days and 22.5 hours.
        </p>
      </MoonPhaseDate>
      <MoonDistance geoPosition={geoPosition} date={date}>
        <p>
          The Moon's average orbital distance is 384,402 km (238,856 mi), or
          1.28 light-seconds. This is about thirty times the diameter of Earth.
          The Moon's apparent size in the sky is almost the same as that of the
          Sun, since the star is about 400 times the lunar distance and
          diameter.
          <br />
          Therefore, the Moon covers the Sun nearly precisely during a total
          solar eclipse. This matching of apparent visual size will not
          continue in the far future because the Moon's distance from Earth is
          gradually increasing.
        </p>
      </MoonDistance>
      <MoonApsis date={date}>
        <p>
          The distance between the Moon and Earth varies from around 356,400 km
          (221,500 mi) to 406,700 km (252,700 mi) at perigee (closest) and
          apogee (farthest), respectively.
        </p>
      </MoonApsis>
      <MoonNode date={date}>
        <p>
          The orbit of the Moon lies in a plane that is inclined about 5.14°
          with respect to the ecliptic plane. The line of intersection of these
          planes passes through the two points at which the Moon's orbit
          crosses the ecliptic plane: the <b>ascending node</b>, where the Moon
          enters the Northern Celestial Hemisphere, and the <b>descending
          node</b>, where the Moon moves into the Southern.
        </p>
      </MoonNode>
      <LunarEclipse date={date}>
        <p>
          Eclipses may occur when the Earth and the Moon are aligned with the
          Sun, and the shadow of one body cast by the Sun falls on the other.
          At full moon, when the Moon is in opposition to the Sun, the Moon may
          pass through the shadow of the Earth, and a lunar eclipse is visible
          from the night half of the Earth.
        </p>
        <p>
          At least two lunar eclipses and as many as five occur every year,
          although total lunar eclipses are significantly less common.
        </p>
      </LunarEclipse>
    </ListSection>,
    <ListSection menuLabel="Sun">
      <SunPosition geoPosition={geoPosition} date={date}>
        <p>
          Sunrises occur approximately due east on the March and September
          equinoxes for all viewers on Earth.
        </p>
      </SunPosition>
      <EarthApsis date={date}>
        <p>
          Currently, the Earth reaches perihelion in early January,
          approximately 14 days after the December solstice. At perihelion, the
          Earth's center is about 0.98329 astronomical units (AU) or
          147,098,070 km (91,402,500 mi) from the Sun's center. In contrast,
          the Earth reaches aphelion currently in early July, approximately 14
          days after the June solstice. The aphelion distance between the
          Earth's and Sun's centers is currently about 1.01671 AU or
          152,097,700 km (94,509,100 mi).
        </p>
        <p>
          At both perihelion and aphelion it is summer in one hemisphere while
          it is winter in the other one. Winter falls on the hemisphere where
          sunlight strikes least directly, and summer falls where sunlight
          strikes most directly, regardless of the Earth's distance from the
          Sun.
        </p>
        <p>
          On a very long time scale, the dates of the perihelion and of the
          aphelion progress through the seasons, and they make one complete
          cycle in 22,000 to 26,000 years.
        </p>
      </EarthApsis>
      <SolarEclipse date={date}>
        <p>
          Eclipses may occur when the Earth and the Moon are aligned with the
          Sun, and the shadow of one body cast by the Sun falls on the other.
          So at new moon, when the Moon is in conjunction with the Sun, the
          Moon may pass in front of the Sun as seen from a narrow region on the
          surface of the Earth and cause a solar eclipse.
        </p>
      </SolarEclipse>
      <EqTime date={date}>
        <p>
          The equation of time describes the discrepancy between two times: the
          apparent solar time, which directly tracks the diurnal motion of the
          Sun, and mean solar time, which tracks a theoretical mean Sun with
          uniform motion.
        </p>
        <p>
          Apparent time, and the sundial, can be ahead (fast) by as much as 16
          min 33 s (around 3 November), or behind (slow) by as much as 14 min 6
          s (around 11 February). The equation of time has zeros near 15 April,
          13 June, 1 September, and 25 December.
        </p>
        <p>
          During the first three months of each year, the clock is ahead of the
          sundial. The mnemonic "NYSS" (pronounced "nice"), for "new year,
          sundial slow", can be useful.
        </p>
      </EqTime>
      <Sundial geoPosition={geoPosition} date={date}>
        <p>
          A sundial is a device that tells the time of day when there is
          sunlight by the apparent position of the Sun in the sky.
        </p>
      </Sundial>
    </ListSection>,
  ];
  const renderSections = () => React.Children.map(sections,
    (section, i) => React.cloneElement(section, {
      key: section.props.menuLabel,
      onFocus: ii => handleSectionFocus(i, ii),
    })
  );
  return (
    <React.StrictMode>
      <header>
        <TabsMenu
          labels={sections.map(section => section.props.menuLabel)}
          active={active}
          onMenuChange={handleMenuChange}
        />
      </header>
      <main>
        <SectionSwicher active={active}>
          {renderSections()}
        </SectionSwicher>
      </main>
    </React.StrictMode>
  );
}

export default App;
