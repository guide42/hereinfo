import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import DateTime from '../display/DateTime';
import Timezone from '../display/Timezone';
import FormControlList from '../ui/FormControlList';
import TimeControl from '../ui/form/TimeControl';
import { pad, tzOffsetToTime, formatISO8601 } from '../helpers';
import './DateTimeAndTimezone.css';

const dateIsValid = date => date.toJSON() !== null;

function DateTimeAndTimezone(props) {
  const { isActive, onToggle, onDateChange } = props;
  const date = props.date ?? new Date();
  const timezone = props.timezone ?? date.getTimezoneOffset();

  const formRef = useRef();

  const handleDateChange = event => {
    if (event.target.value.length === 0) {
      return;
    }

    const newDate = new Date(
      event.target.value
      + "T" + pad(date.getHours())
      + ":" + pad(date.getMinutes())
      + ":00"
      + tzOffsetToTime(timezone)
    );

    if (dateIsValid(newDate)) {
      onDateChange(newDate);
    }
  };

  const handleHourChange = event => {
    if (event.target.value.length === 0) {
      return;
    }

    const newDate = new Date(
      formatISO8601(date)
      + "T" + pad(event.target.value)
      + ":" + pad(date.getMinutes())
      + ":00"
      + tzOffsetToTime(timezone)
    );

    if (dateIsValid(newDate)) {
      onDateChange(newDate);
    }
  };

  const handleMinuteChange = event => {
    if (event.target.value.length === 0) {
      return;
    }

    const newDate = new Date(
      formatISO8601(date)
      + "T" + pad(date.getHours())
      + ":" + pad(event.target.value)
      + ":00"
      + tzOffsetToTime(timezone)
    );

    if (dateIsValid(newDate)) {
      onDateChange(newDate);
    }
  };

  const handleNowClick = event => {
    onDateChange(new Date());
    onToggle();
  };

  const handleSubmit = event => {
    event.preventDefault();
  };

  return (
    <div className="datetime-and-timezone">
      <label>Date</label>
      <DateTime date={date} />
      &nbsp;
      <Timezone timezone={timezone} />
      <dialog
        open={isActive}
        aria-hidden={!isActive}
        style={{display: isActive ? "block" : "none"}}
      >
        <form method="dialog" onSubmit={handleSubmit} ref={formRef}>
          <FormControlList
            isActive={isActive}
            onToggle={onToggle}
            formRef={formRef}
          >
            <input
              type="date"
              value={formatISO8601(date)}
              onChange={handleDateChange}
              autoFocus={true}
            />
            <TimeControl
              hour={date.getHours()}
              minute={date.getMinutes()}
              onHourChange={handleHourChange}
              onMinuteChange={handleMinuteChange}
            />
            <button className="btn now" onClick={handleNowClick}>
              Now
            </button>
          </FormControlList>
        </form>
      </dialog>
    </div>
  );
}

DateTimeAndTimezone.defaultProps = {
  isActive: false,
};

DateTimeAndTimezone.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  onDateChange: PropTypes.func.isRequired,
  date: PropTypes.instanceOf(Date),
  timezone: PropTypes.number,
};

export default DateTimeAndTimezone;
