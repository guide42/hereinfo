import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import DateTime from '../display/DateTime';
import './Event.css';

function Event(props) {
  const { name, date, limit, isActive, children, onToggle, handleClose } = props;

  const handleKeyUp = useCallback(
    event => {
      if (handleClose && isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [handleClose, isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  return (
    <>
      <div className="event">
        <label>{name}</label>
        <DateTime date={date} limit={limit} />
      </div>
      {React.Children.count(children) > 0 && (
        <article style={{ display: isActive ? "block" : "none" }}>
          {children}
        </article>
      )}
    </>
  );
}

Event.defaultProps = {
  isActive: false,
  handleClose: true,
};

Event.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  handleClose: PropTypes.bool,
  name: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  limit: PropTypes.string,
};

export default Event;
