import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import './MoreText.css';

function MoreText(props) {
  const { isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const renderChildren = () => {
    if (isActive) {
      return props.children;
    }

    let firstParagraphFound = false;

    return React.Children.toArray(props.children).map((element) => {
      if (!firstParagraphFound) {
        if (element.type === 'p') {
          firstParagraphFound = true;
          return React.cloneElement(element, {
            className: "first",
          });
        }
        return element;
      }
      return null;
    });
  };

  return (
    <div className="more-text">
      <article>
        {renderChildren()}
      </article>
    </div>
  );
}

MoreText.defaultProps = {
  isActive: false,
};

MoreText.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
};

export default MoreText;
