import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import SunCalc from 'suncalc';
import './MoonTime.css';

const pad = n => n < 10 ? '0' + n : n;

function formatTime(date) {
  return `${pad(date.getHours())}:${pad(date.getMinutes())}`;
}

function MoonTime(props) {
  const { date, geoPosition, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const times = SunCalc.getMoonTimes(
    date,
    geoPosition.latitude,
    geoPosition.longitude
  );

  const { phase } = SunCalc.getMoonIllumination(
    date,
    geoPosition.latitude,
    geoPosition.longitude
  );

  let today;
  if (times.rise || times.set) {
    today = (
      <>
        {
          times.rise
          ? <span className="start">{formatTime(times.rise)}</span>
          : <span className="yesterday">(yesterday)</span>
        }
        &nbsp;&mdash;&nbsp;
        {
          times.set
          ? <span className="end">{formatTime(times.set)}</span>
          : <span className="tomorrow">(tomorrow)</span>
        }
      </>
    );
  } else if (times.alwaysUp) {
    today =
      <span className="always-up">Always <em>above</em> the horizon.</span>;
  } else if (times.alwaysDown) {
    today =
      <span className="always-down">Always <em>below</em> the horizon.</span>;
  }

  let visibility;
  if (phase === 0) {
    visibility = "Invisible (too close to Sun.)";
  } else if (phase > 0 && phase < 0.25) {
    visibility = "Late morning to post-dusk.";
  } else if (phase === 0.25) {
    visibility = "Afternoon and early evening.";
  } else if (phase > 0.25 && phase < 0.5) {
    visibility = "Late afternoon and most of night.";
  } else if (phase === 0.5) {
    visibility = "Sunset to sunrise (all night.)";
  } else if (phase > 0.5 && phase < 0.75) {
    visibility = "Most of night and early morning.";
  } else if (phase === 0.75) {
    visibility = "Late night and morning.";
  } else if (phase > 0.75 && phase < 1) {
    visibility = "Pre-dawn to early afternoon.";
  } else {
    visibility = "Forgotten";
  }

  let earthshine;
  if ((phase > 0 && phase < 0.21) && (phase > 0.79 && phase < 1)) {
    earthshine = <p><label>Earthshine</label> best visible.</p>;
  }

  return (
    <div className="moontime">
      <label>Moon</label>
      {today}
      <article style={{ display: isActive ? "block" : "none" }}>
        <p><label>Visibility</label> {visibility}</p>
        {earthshine}
        {props.children}
      </article>
    </div>
  );
}

MoonTime.defaultProps = {
  isActive: false,
};

MoonTime.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
  geoPosition: PropTypes.exact({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    altitude: PropTypes.number,
  }).isRequired,
};

export default MoonTime;
