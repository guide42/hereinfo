import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { newMoon, full } from 'astronomia/src/moonphase';
import { Calendar, JDEToDate } from 'astronomia/src/julian';
import Event from '../Event';
import './MoonPhaseDate.css';

function MoonPhaseDate(props) {
  const { date, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const year = new Calendar(date).toYear();

  const newDate = JDEToDate(newMoon(year));
  const fullDate = JDEToDate(full(year));

  return (
    <>
      <div className="moon-phase-date">
        <Event name="New Moon" date={newDate} />
        <Event name="Full Moon" date={fullDate} />
        <article style={{ display: isActive ? "block" : "none" }}>
          {props.children}
        </article>
      </div>
    </>
  );
}

MoonPhaseDate.defaultProps = {
  isActive: false,
};

MoonPhaseDate.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
};

export default MoonPhaseDate;
