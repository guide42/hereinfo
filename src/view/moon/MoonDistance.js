import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import SunCalc from 'suncalc';
import { formatDistance } from '../../helpers';
import './MoonDistance.css';

function MoonDistance(props) {
  const { date, geoPosition, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const { distance } = SunCalc.getMoonPosition(
    date,
    geoPosition.latitude,
    geoPosition.longitude
  );

  return (
    <>
      <div className="moon-distance">
        <label>Moon Distance</label>
        <span className="distance">{formatDistance(distance)}</span>
        <article style={{ display: isActive ? "block" : "none" }}>
          {props.children}
        </article>
      </div>
    </>
  );
}

MoonDistance.defaultProps = {
  isActive: false,
};

MoonDistance.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
  geoPosition: PropTypes.exact({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    altitude: PropTypes.number,
  }).isRequired,
};

export default MoonDistance;
