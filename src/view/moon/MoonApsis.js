import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { perigee, apogee, apogeeParallax, perigeeParallax, distance } from 'astronomia/src/apsis';
import { Calendar, JDToDate } from 'astronomia/src/julian';
import { formatDistance } from '../../helpers';
import Event from '../Event';
import './MoonApsis.css';

function MoonApsis(props) {
  const { date, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace') {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const year = new Calendar(date).toYear();

  const perigeeDate = JDToDate(perigee(year));
  const apogeeDate = JDToDate(apogee(year));

  const perigeeDistance = distance(perigeeParallax(year));
  const apogeeDistance = distance(apogeeParallax(year));

  return (
    <>
      <div className="moon-apsis">
        <Event name="Perigee" date={perigeeDate}>
          <p className="distance">{formatDistance(perigeeDistance)}</p>
        </Event>
        <Event name="Apogee" date={apogeeDate}>
          <p className="distance">{formatDistance(apogeeDistance)}</p>
        </Event>
        <article style={{ display: isActive ? "block" : "none" }}>
          {props.children}
        </article>
      </div>
    </>
  );
}

MoonApsis.defaultProps = {
  isActive: false,
};

MoonApsis.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
};

export default MoonApsis;
