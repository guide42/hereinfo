import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { ascending, descending } from 'astronomia/src/moonnode';
import { Calendar, JDEToDate } from 'astronomia/src/julian';
import Event from '../Event';
import './MoonNode.css';

function MoonNode(props) {
  const { date, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace') {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const year = new Calendar(date).toYear();

  const ascendingDate = JDEToDate(ascending(year));
  const descendingDate = JDEToDate(descending(year));

  return (
    <>
      <div className="moon-node">
        <Event name="Ascending" date={ascendingDate} />
        <Event name="Descending" date={descendingDate} />
        <article style={{ display: isActive ? "block" : "none" }}>
          {props.children}
        </article>
      </div>
    </>
  );
}

MoonNode.defaultProps = {
  isActive: false,
};

MoonNode.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
};

export default MoonNode;
