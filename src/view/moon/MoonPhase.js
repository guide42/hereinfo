import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import SunCalc from 'suncalc';
import './MoonPhase.css';

const formatPercentage = fraction => `${(fraction * 100).toFixed(2)}%`;

const formatDescription =
  (side, fraction) => `${side} side, ${formatPercentage(fraction)} lit disc.`;

const formatAge = (age) => `${Math.trunc(age)} days`;

function MoonPhase(props) {
  const { date, geoPosition, isActive, onToggle } = props;

  const { fraction, phase } = SunCalc.getMoonIllumination(date);

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const age = phase * 29.530588853;

  let phaseName, phaseKey;
  if (phase === 0) {
    phaseName = "New Moon";
    phaseKey = "new-moon";
  } else if (phase > 0 && phase < 0.25) {
    phaseName = "Waxing Crescent";
    phaseKey = "waxing-crescent";
  } else if (phase === 0.25) {
    phaseName = "First Quarter";
    phaseKey = "first-quarter";
  } else if (phase > 0.25 && phase < 0.5) {
    phaseName = "Waxing Gibbous";
    phaseKey = "waxing-gibbous";
  } else if (phase === 0.5) {
    phaseName = "Full Moon";
    phaseKey = "full-moon";
  } else if (phase > 0.5 && phase < 0.75) {
    phaseName = "Waning Gibbous";
    phaseKey = "waning-gibbous";
  } else if (phase === 0.75) {
    phaseName = "Last Quarter";
    phaseKey = "last-quarter";
  } else if (phase > 0.75 && phase < 1) {
    phaseName = "Waning Crescent";
    phaseKey = "waning-crescent";
  } else {
    phaseName = "\"Old\" Moon";
    phaseKey = "new-moon";
  }

  let description;
  if (phase === 0) {
    description = "Disc completely in Sun's shadow (lit by earthshine only.)";
  } else if (phase > 0 && phase < 0.5) {
    if (geoPosition.latitude > 0) {
      description = formatDescription('Right', fraction);
    } else if (geoPosition.latitude < 0) {
      description = formatDescription('Left', fraction);
    }
  } else if (phase === 0.5) {
    description = "100% illuminated disc.";
  } else {
    if (geoPosition.latitude > 0) {
      description = formatDescription('Left', fraction);
    } else if (geoPosition.latitude < 0) {
      description = formatDescription('Right', fraction);
    }
  }
  if (!description) {
    description = `${formatPercentage(fraction)} lit disc.`;
  }

  const southRight = geoPosition.latitude < 0 ? 'right' : 'left';
  const southLeft = geoPosition.latitude < 0 ? 'left' : 'right';

  return (
    <div className="moon-phase">
      <div className="phase">
        <svg className="moon" width="64" height="64" viewBox="0 0 64 64">
          <clipPath id="half-left">
            <rect x="0" y="0" width="32" height="64" fill="white" />
          </clipPath>
          <clipPath id="half-right">
            <rect x="32" y="0" width="32" height="64" fill="white" />
          </clipPath>
          <mask id="new-moon">
            <circle cx="32" cy="32" r="32" fill="white" />
          </mask>
          <mask id="waxing-crescent">
            <circle cx="32" cy="32" r="32" fill="white"
              clipPath={`url(#half-${southRight})`}
            />
            <ellipse cx="32" cy="32" rx="16" ry="32" fill="white" />
          </mask>
          <mask id="first-quarter">
            <circle cx="32" cy="32" r="32" fill="white" />
            <circle cx="32" cy="32" r="32" fill="black"
              clipPath={`url(#half-${southLeft})`}
            />
          </mask>
          <mask id="waxing-gibbous">
            <circle cx="32" cy="32" r="32" fill="white" />
            <circle cx="32" cy="32" r="32" fill="black"
              clipPath={`url(#half-${southLeft})`}
            />
            <ellipse cx="32" cy="32" rx="16" ry="32" fill="black" />
          </mask>
          <mask id="full-moon">
            <circle cx="32" cy="32" r="32" fill="black" />
          </mask>
          <mask id="waning-gibbous">
            <circle cx="32" cy="32" r="32" fill="white" />
            <circle cx="32" cy="32" r="32" fill="black"
              clipPath={`url(#half-${southRight})`}
            />
            <ellipse cx="32" cy="32" rx="16" ry="32" fill="black" />
          </mask>
          <mask id="last-quarter">
            <circle cx="32" cy="32" r="32" fill="white" />
            <circle cx="32" cy="32" r="32" fill="black"
              clipPath={`url(#half-${southRight})`}
            />
          </mask>
          <mask id="waning-crescent">
            <circle cx="32" cy="32" r="32" fill="white"
              clipPath={`url(#half-${southLeft})`}
            />
            <ellipse cx="32" cy="32" rx="16" ry="32" fill="white" />
          </mask>
          <circle cx="32" cy="32" r="32" fill="white" />
          <circle cx="32" cy="32" r="32" fill="black"
            mask={`url(#${phaseKey}`}
          />
        </svg>
        <div className="info">
          <p>
            <label>Phase</label>
            <span className="fraction">{phaseName}</span>
          </p>
          <p>
            <label>Age</label>
            <span className="age">{formatAge(age)}</span>
          </p>
          <p>{description}</p>
        </div>
      </div>
      <article style={{ display: isActive ? "block" : "none" }}>
        <p><label>Illuminated</label> {fraction * 100}%</p>
        {
          geoPosition.latitude &&
          geoPosition.latitude > 0 &&
          <>
            <p>
              The right side of the Moon is the part that is always waxing.
              (That is, if the right side is dark, the Moon is becoming darker;
              if the right side is lit, the Moon is getting brighter.)
            </p>
            <p>
              "DOC" represents phases of the Moon by shape: "D" is the waxing
              moon; "O" the full moon; and "C" represent the waning moon.
              A simple English saying is "Dog comes in, Cat goes out".
            </p>
          </>
        }
        {
          geoPosition.latitude &&
          geoPosition.latitude < 0 &&
          <>
            <p>
              The left side of the Moon is the part that is always waxing.
              (That is, if the left side is dark, the Moon is becoming darker;
              if the left side is lit, the Moon is getting brighter.)
            </p>
            <p>
              "COD" represents phases of the Moon by shape: "C" is the waxing
              moon; "O" the full moon; and "D" represent the waning moon.
            </p>
          </>
        }
        {props.children}
      </article>
    </div>
  );
}

MoonPhase.defaultProps = {
  isActive: false,
};

MoonPhase.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
  geoPosition: PropTypes.exact({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    altitude: PropTypes.number,
  }).isRequired,
};

export default MoonPhase;
