import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { TYPE, lunar } from 'astronomia/src/eclipse';
import { Calendar, JDEToDate } from 'astronomia/src/julian';
import DateTime from '../../display/DateTime';
import './LunarEclipse.css';

function LunarEclipse(props) {
  const { date, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const year = new Calendar(date).toYear();

  const { type, jdeMax } = lunar(year);

  let eclipseDate;
  if (type !== TYPE.None) {
    eclipseDate = JDEToDate(jdeMax);
  }

  return (
    <>
      <div className="lunar-eclipse">
        {
          type === TYPE.None
          ? <p className="none">No lunar eclipses</p>
          : (
            <div className="lunar">
              <div className="info">
                <p>
                  {type === TYPE.Total && <b>Total lunar eclipse</b>}
                  {type === TYPE.Penumbral && <b>Penumbral lunar eclipse</b>}
                  {type === TYPE.Umbral && <b>Umbral lunar eclipse</b>}
                </p>
                <p>
                  <DateTime date={eclipseDate} />
                </p>
              </div>
            </div>
          )
        }
        <article style={{ display: isActive ? "block" : "none" }}>
          <p>
            Earth's shadow can be divided into two distinctive parts: the umbra
            and penumbra. Earth totally occludes direct solar radiation within
            the umbra, the central region of the shadow. However, since the
            Sun's diameter appears about one-quarter of Earth's in the lunar
            sky, the planet only partially blocks direct sunlight within the
            penumbra, the outer portion of the shadow.
          </p>
          {type === TYPE.Total &&
            <p>
              A totally eclipsed Moon is sometimes called a blood moon due to
              its reddish color, which is caused by Earth completely blocking
              direct sunlight from reaching the Moon. The only light reflected
              from the lunar surface has been refracted by Earth's atmosphere.
            </p>
          }
          {type === TYPE.Penumbral &&
            <p>
              A penumbral lunar eclipse occurs when the Moon passes through
              Earth's penumbra. The penumbra causes a subtle dimming of the
              lunar surface, which is only visible to the naked eye when about
              70% of the Moon's diameter has immersed into Earth's penumbra.
            </p>
          }
          {type === TYPE.Umbral &&
            <>
              <p>
                A partial lunar eclipse occurs when only a portion of the Moon
                enters Earth's umbra, while a total lunar eclipse occurs when
                the entire Moon enters the planet's umbra.
              </p>
              <p>
                The Moon does not completely darken as it passes through the
                umbra because of the refraction of sunlight by Earth's atmosphere
                into the shadow cone; if Earth had no atmosphere, the Moon would
                be completely dark during the eclipse.
              </p>
            </>
          }
          {props.children}
        </article>
      </div>
    </>
  );
}

LunarEclipse.defaultProps = {
  isActive: false,
};

LunarEclipse.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
};

export default LunarEclipse;
