import React from 'react';
import PropTypes from 'prop-types';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { Coord, approxAngularDistance, approxLinearDistance } from 'astronomia/src/globe';
import { formatLatitude, formatLongitude, formatDistance } from '../helpers';
import FormControlList from '../ui/FormControlList';
import './GeoPositionHistory.css';

function calcDistance(lat0, lon0, lat1, lon1) {
  return approxLinearDistance(
    Math.acos(
      approxAngularDistance(
        new Coord(lat0, lon0),
        new Coord(lat1, lon1)
      )
    )
  );
}

class GeoPositionHistory extends React.Component {
  constructor(props) {
    super(props);
    this.formRef = React.createRef();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePrevClick = this.handlePrevClick.bind(this);
    this.handleNextClick = this.handleNextClick.bind(this);
    this.handleSaveClick = this.handleSaveClick.bind(this);
    this.state = {
      last: undefined,
      history: [],
      error: undefined,
      isLoaded: false,
      current: null,
    };
  }

  setHistory(history) {
    const last = history.pop();
    this.setState({
      last,
      history,
      error: undefined,
      isLoaded: true,
    });
  }

  componentDidMount() {
    if (!localStorage) {
      this.setState({error: <>Unavailable</>});
    } else if (!localStorage.getItem('history')) {
      this.setState({error: <>Unset</>});
    } else {
      const data = localStorage.getItem('history');
      const json = JSON.parse(data);
      if (Array.isArray(json)) {
        if (json.length === 0) {
          this.setState({error: <>Empty</>});
        } else {
          this.setHistory(json);
        }
      } else {
        this.setState({error: <>Invalid</>});
      }
    }
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  handlePrevClick(event) {
    const index = this.state.current - 1;
    if (index >= -1) {
      event.preventDefault();
      const pos = this.getPos(index);
      this.props.onPositionChange({
        latitude: pos.t,
        longitude: pos.n,
        altitude: pos.a,
      });
      this.setState({current: index});
    }
  }

  handleNextClick(event) {
    const index = this.state.current === null ? -1 : this.state.current + 1;
    if (index < this.state.history.length) {
      event.preventDefault();
      const pos = this.getPos(index);
      this.props.onPositionChange({
        latitude: pos.t,
        longitude: pos.n,
        altitude: pos.a,
      });
      this.setState({current: index});
    }
  }

  handleSaveClick(event) {
    if (this.props.latitude !== null && this.props.longitude !== null) {
      const newHistory = [].concat(
        this.state.history,
        this.state.last ? [this.state.last] : [],
        [{
          d: this.props.date.getTime(),
          t: this.props.latitude,
          n: this.props.longitude,
          a: this.props.altitude,
        }]
      );
      if (localStorage) {
        localStorage.setItem('history', JSON.stringify(newHistory));
      }
      this.setHistory(newHistory);
    }
  }

  getPos(index) {
    if (index === -1) {
      return this.state.last;
    }
    return this.state.history[index];
  }

  withDialog(element) {
    return (
      <>
        {element}
        <dialog
          open={this.props.isActive}
          aria-hidden={!this.props.isActive}
          style={{display: this.props.isActive ? "block" : "none"}}
        >
          <form
            method="dialog"
            onSubmit={this.handleSubmit}
            ref={this.formRef}
          >
            <FormControlList
              isActive={this.props.isActive}
              onToggle={this.props.onToggle}
              formRef={this.formRef}
            >
              {this.renderPrevButton()}
              {this.renderNextButton()}
              {
                this.props.latitude && this.props.longitude
                  ? <button className="btn save" onClick={this.handleSaveClick}>
                      Save{" "}
                      {formatLatitude(this.props.latitude)},{" "}
                      {formatLongitude(this.props.longitude)}
                    </button>
                  : <small className="error">
                      Geographic position not available
                    </small>
              }
            </FormControlList>
          </form>
        </dialog>
      </>
    );
  }

  renderPrevButton() {
    if (this.state.current !== null && this.state.current - 1 >= -1) {
      const pos = this.getPos(this.state.current - 1);
      return (
        <button className="btn prev" onClick={this.handlePrevClick}>
          Prev{" "}
          {formatLatitude(pos.t)},{" "}
          {formatLongitude(pos.n)}
        </button>
      );
    }
    return <></>;
  }

  renderNextButton() {
    if (
      (this.state.current === null && this.state.last !== undefined) ||
      (this.state.current !== null && this.state.current + 1 < this.state.history.length)
    ) {
      const pos = this.getPos(
        this.state.current === null ? -1 : this.state.current + 1
      );
      return (
        <button className="btn next" onClick={this.handleNextClick}>
          Next{" "}
          {formatLatitude(pos.t)},{" "}
          {formatLongitude(pos.n)}
        </button>
      );
    }
    return <></>;
  }

  renderError(msg) {
    return (
      <>
        <label>History</label>
        <span className="error">{msg}</span>
      </>
    );
  }

  renderLoading() {
    return (
      <>
        <label>History</label>
        <span className="loading">Loading...</span>
      </>
    );
  }

  renderLast(last) {
    return (
      <>
        <label>Last Pos.</label>
        <span className="latitude">{last.t}</span>,{" "}
        <span className="longitude">{last.n}</span>
      </>
    );
  }

  renderHistory(last, history) {
    return (
      <>
        <label>History</label>
        <table cellPadding="0" cellSpacing="0" border="0" className="history">
          <thead>
            <tr>
              <th className="date">Ago</th>
              <th className="pos">Distance</th>
            </tr>
          </thead>
          <tbody>
            {this.renderPosition(last)}
            {history.map(
              (pos) => this.renderPosition(pos)
            )}
          </tbody>
        </table>
      </>
    );
  }

  renderPosition(pos) {
    const uniq = pos.d + pos.t + pos.n;
    return (
      <tr key={uniq}>
        <td className="date">
          {formatDistanceToNow(new Date(pos.d))}
        </td>
        <td className="pos">
          {formatDistance(calcDistance(
            pos.t, pos.n,
            this.props.latitude, this.props.longitude
          ))}
        </td>
      </tr>
    );
  }

  render() {
    if (this.state.error) {
      return this.withDialog(this.renderError(this.state.error));
    } else if (!this.state.isLoaded) {
      return this.withDialog(this.renderLoading());
    } else if (!this.props.isActive) {
      return this.withDialog(this.renderLast(this.state.last));
    } else {
      return this.withDialog(
        this.renderHistory(this.state.last, this.state.history)
      );
    }
  }
}

GeoPositionHistory.defaultProps = {
  isActive: false,
};

GeoPositionHistory.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  onPositionChange: PropTypes.func.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  altitude: PropTypes.number,
};

export default GeoPositionHistory;
