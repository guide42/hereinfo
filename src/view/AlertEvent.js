import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import subMinutes from 'date-fns/subMinutes';
import isPast from 'date-fns/isPast';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { notify } from '../alarms';
import Event from '../view/Event';
import FormControlList from '../ui/FormControlList';
import './AlertEvent.css';

const DEFAULT_MINUTES = 60;

function AlertEvent(props) {
  const { name, date, isActive, onToggle, initialMinutes, children } = props;
  const [minutes, setMinutes] = useState(initialMinutes || DEFAULT_MINUTES);
  const [errors, setErrors] = useState(null);
  const formRef = useRef();

  useEffect(
    () => {
      if (errors === null) {
        const newErrors = [];
        if (isPast(date)) {
          newErrors.push("Event is in the past.");
        }
        if (!navigator.mozAlarms) {
          newErrors.push("Alarm system unavailable.");
        }
        if (!navigator.mozSetMessageHandler) {
          newErrors.push("Message system unavailable.");
        }
        if (!Notification) {
          newErrors.push("Notification system unavailable.");
        }
        setErrors(newErrors);
      }
    },
    [date, errors, setErrors]
  );

  const addAlert = () => {
    const alarmDate = subMinutes(date, minutes);
    const alarmBody = props.text ? props.text : children;
    const request = navigator.mozAlarms.add(
      alarmDate,
      'honorTimezone',
      notify(`${name} in ${minutes} minutes`, alarmBody)
    );
    request.onsuccess = () => {
      alert(`${name} alarm set in ${formatDistanceToNow(alarmDate)}`);
    };
  };

  const handleSubmit = event => {
    event.preventDefault();
  };

  const handleMinutesChange = event => {
    if (event.target.value.length === 0) {
      return;
    }
    const minutes = parseInt(event.target.value);
    if (!isNaN(minutes)) {
      setMinutes(minutes);
    }
  };

  const handleAddRemainder = event => {
    if (errors !== null && errors.length === 0) {
      if (Notification.permission === 'granted') {
        addAlert();
      } else if (Notification.permission !== 'denied') {
        Notification.requestPermission().then((permission) => {
          if (permission === 'granted') {
            addAlert();
          }
        });
      }
    }
    onToggle && onToggle();
  };

  return (
    <Event {...props} handleClose={!!errors}>
      {children}
      <dialog
        open={isActive}
        aria-hidden={!isActive}
      >
        {errors
          ? errors.map(
            error => <p key={error} className="error">{error}</p>
          )
          : (
            <form method="dialog" onSubmit={handleSubmit} ref={formRef}>
              <FormControlList
                isActive={isActive}
                onToggle={onToggle}
                formRef={formRef}
              >
                <button className="btn" onClick={handleAddRemainder}>
                  Add Remainder
                </button>
                <>
                  <input
                    type="number"
                    className="event-minutes"
                    value={minutes}
                    min="1" step="1"
                    onChange={handleMinutesChange}
                  />
                  {" "}minutes before.
                </>
              </FormControlList>
            </form>
          )
        }
      </dialog>
    </Event>
  );
}

AlertEvent.defaultProps = {
  isActive: false,
};

AlertEvent.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  name: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  limit: PropTypes.string,
  initialMinutes: PropTypes.number,
  text: PropTypes.string,
};

export default AlertEvent;
