import React from 'react';
import PropTypes from 'prop-types';
import GeoPosition from '../display/GeoPosition';
import FormControlList from '../ui/FormControlList';
import './GeoLocation.css';

const hasGeolocation =
  typeof navigator !== 'undefined' && 'geolocation' in navigator;

const maxTimeouts = 3;

class GeoLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {errorCode: 0, retryTimeout: 0};
    this.handleAutoDetect = this.handleAutoDetect.bind(this);
    this.handleLatitudeChange = this.handleLatitudeChange.bind(this);
    this.handleLongitudeChange = this.handleLongitudeChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.formRef = React.createRef();
  }

  componentDidMount() {
    this.getCurrentPosition();
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.latitude !== null &&
      this.props.latitude !== prevProps.latitude &&
      this.props.longitude !== null &&
      this.props.longitude !== prevProps.longitude
    ) {
      this.setState({errorCode: 0, retryTimeout: 0});
    }
  }

  getCurrentPosition() {
    if (!hasGeolocation) {
      return;
    }
    if (
      !this.props.latitude &&
      !this.props.longitude &&
      this.state.errorCode === 0
    ) {
      navigator.geolocation.getCurrentPosition(
        newPosition => {
          this.props.onPositionChange({
            latitude: newPosition.coords.latitude,
            longitude: newPosition.coords.longitude,
            altitude: newPosition.coords.altitude,
          });
        },
        newError => {
          if (
            newError.code === 3 &&
            this.state.retryTimeout + 1 <= maxTimeouts
          ) {
            this.setState({retryTimeout: this.state.retryTimeout + 1});
          } else {
            this.setState({errorCode: newError.code});
          }
        },
        {
          maximumAge: 0,
          timeout: 30000 + (this.state.retryTimeout * 30000),
          enableHighAccuracy: true,
        }
      );
    }
  }

  handleAutoDetect(event) {
    this.setState({errorCode: 0, retryTimeout: 0});
    if ('onToggle' in this.props) {
      this.props.onToggle();
    }
    this.props.onPositionChange({
      latitude: null,
      longitude: null,
      altitude: null,
    });
  }

  handleLatitudeChange(event) {
    const newLatitude = parseFloat(event.target.value);
    if (isNaN(newLatitude)) {
      return;
    }
    this.setState({errorCode: 0, retryTimeout: 0});
    this.props.onPositionChange({
      latitude: newLatitude,
      longitude: this.props.longitude,
      altitude: null,
    });
  }

  handleLongitudeChange(event) {
    const newLongitude = parseFloat(event.target.value);
    if (isNaN(newLongitude)) {
      return;
    }
    this.setState({errorCode: 0, retryTimeout: 0});
    this.props.onPositionChange({
      latitude: this.props.latitude,
      longitude: newLongitude,
      altitude: null,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  errorMessage(msg) {
    return (
      <>
        <span className="error">{msg}</span>
      </>
    );
  }

  withDialog(element) {
    return (
      <>
        <label>Position</label>
        {element}
        <dialog
          open={this.props.isActive}
          aria-hidden={!this.props.isActive}
          style={{display: this.props.isActive ? "block" : "none"}}
        >
          <form
            method="dialog"
            onSubmit={this.handleSubmit}
            ref={this.formRef}
          >
            <FormControlList
              isActive={this.props.isActive}
              onToggle={this.props.onToggle}
              formRef={this.formRef}
            >
              <input
                type="number"
                value={this.props.latitude ?? ""}
                min="-180" max="180" step="0.0000000000001"
                placeholder="Latitude"
                onChange={this.handleLatitudeChange}
              />
              <input
                type="number"
                value={this.props.longitude ?? ""}
                min="-180" max="180" step="0.0000000000001"
                placeholder="Longitude"
                onChange={this.handleLongitudeChange}
              />
              <button className="btn" onClick={this.handleAutoDetect}>
                Auto-detect
              </button>
            </FormControlList>
          </form>
        </dialog>
      </>
    );
  }

  render() {
    if (!hasGeolocation) {
      return this.withDialog(
        this.errorMessage("Geolocation unavailable")
      );
    }
    if (this.state.errorCode > 0) {
      switch (this.state.errorCode) {
        case 1:
          return this.withDialog(
            this.errorMessage("Please allow geolocation permissions")
          );
        case 2:
          return this.withDialog(
            this.errorMessage("Geolocation failed")
          );
        case 3:
          return this.withDialog(
            this.errorMessage("Geolocation took too long")
          );
        default:
          return this.withDialog(
            this.errorMessage("Unavailable: Unknown error")
          );
      }
    }
    if (!this.props.latitude && !this.props.longitude) {
      return this.withDialog(
        <span className="loading">
          Seeking geolocation...
          {
            this.state.retryTimeout > 0
              ? (
                this.state.retryTimeout + " " + (
                  this.state.retryTimeout === 1 ? "try" : "tries"
                )
              ) : ''
          }
        </span>
      );
    }
    return this.withDialog(
      <GeoPosition
        latitude={this.props.latitude}
        longitude={this.props.longitude}
        altitude={this.props.altitude}
      />
    );
  }
}

GeoLocation.defaultProps = {
  isActive: false,
};

GeoLocation.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  onPositionChange: PropTypes.func.isRequired,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  altitude: PropTypes.number,
};

export default GeoLocation;
