import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import isBefore from 'date-fns/isBefore';
import isAfter from 'date-fns/isAfter';
import set from 'date-fns/set';
import format from 'date-fns/format';
import { Calendar } from 'astronomia/src/julian';
import { Sunrise } from 'astronomia/src/sunrise';
import { horizontal } from 'astronomia/src/sundial';
import './Sundial.css';

const r = Math.round;
const c = ['blue', 'green', 'yellow', 'red', 'orange', 'pink', 'purple'];

function Sundial(props) {
  const { date, geoPosition, isActive, onToggle, children } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const today = (() => {
    const sun = new Sunrise(
      new Calendar(date),
      geoPosition.latitude,
      geoPosition.longitude * -1
    );

    const sunrise = sun.rise();
    const sunset = sun.set();

    return {
      start: sunrise
        ? sunrise.toDate()
        : set(date, {hours: 6, minutes: 0, seconds: 0}),
      end: sunset
        ? sunset.toDate()
        : set(date, {hours: 18, minutes: 0, seconds: 0}),
    };
  })();

  const renderSundial = () => {
    const { center, lines } = horizontal(geoPosition.latitude, 10);
    const shadow = lines[date.getHours() - 1];

    return (
      <svg className="horizontal" width="224" height="224" viewBox="0 0 224 224">
        {lines.map(
          ({ hour, points }) => (
            <React.Fragment key={hour}>
              <text
                x={r(112 + points[0].x * 2)}
                y={r(112 + points[0].y * 2)}
                style={{fontSize: "70%", color: "grey"}}
              >
                {hour + 1}
              </text>
              <line
                x1={r(112 + center.x)}
                y1={r(112 + center.y)}
                x2={r(112 + points[0].x)}
                y2={r(112 + points[0].y)}
                stroke="grey"
              />
            </React.Fragment>
          )
        )}
        {shadow && shadow.points.map(
          ({ x, y }, index) => (
            <line
              key={x + 'x' + y + 'y'}
              x1={r(112 + center.x)}
              y1={r(112 + center.y)}
              x2={r(112 + x)}
              y2={r(112 + y)}
              stroke={c[index]}
            />
          )
        )}
        <circle r="1" fill="red"
          cx={r(112 + center.x)}
          cy={r(112 + center.y)}
        />
      </svg>
    );
  };

  return (
    <>
      <div className="sundial">
        {isAfter(date, today.start) && isBefore(date, today.end)
          ? renderSundial()
          : (
            <p className="error">
              Sundial can only be used between <b>{format(today.start, 'p')}</b>
              {" "}and <b>{format(today.end, 'p')}</b>.
            </p>
          )
        }
      </div>
      <article style={{ display: isActive ? "block" : "none" }}>
        {children}
      </article>
    </>
  );
}

Sundial.defaultProps = {
  isActive: false,
};

Sundial.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
  geoPosition: PropTypes.exact({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    altitude: PropTypes.number,
  }).isRequired,
};

export default Sundial;
