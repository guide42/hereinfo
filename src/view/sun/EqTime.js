import React, { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import { DateToJDE } from 'astronomia/src/julian';
import { eSmart } from 'astronomia/src/eqtime';
import { HourAngle } from 'astronomia/src/sexagesimal';
import './EqTime.css';

function EqTime(props) {
  const { date, isActive, onToggle, children } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const rad = eSmart(DateToJDE(date));
  const angle = new HourAngle(rad);

  const hour = Math.abs(angle.hour());
  const minutes = (hour - Math.trunc(hour)) * 60;
  const seconds = (minutes - Math.trunc(minutes)) * 60;

  const diff = (
    (minutes === 0 ? "" : `${Math.trunc(minutes)} minutes`) + " " +
    (seconds === 0 ? "" : `${Math.trunc(seconds)} seconds`)
  ).trim();

  const phrase = rad > 0
    ? `Sun is ${diff} ahead of the clock.`
    : `Clock is ${diff} ahead of the Sun.`;

  return (
    <>
      <div className="eq-time">
        <label>Eq. of Time</label>
        {phrase}
      </div>
      <article style={{ display: isActive ? "block" : "none" }}>
        <p>
          <label>Hour Angle</label>
          <span className="angle">{angle.toString()}</span>
        </p>
        {children}
      </article>
    </>
  );
}

EqTime.defaultProps = {
  isActive: false,
};

EqTime.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
};

export default EqTime;
