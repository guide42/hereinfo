import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import SunCalc from 'suncalc';
import './SunTime.css';

const pad = n => n < 10 ? '0' + n : n;

function formatTime(date) {
  return `${pad(date.getHours())}:${pad(date.getMinutes())}`;
}

const splitCamelCase = s =>
  s.replace(/([A-Z][a-z])/g, " $1")
   .replace(/\s*([A-Z]+)/g, " $1")
   .replace(/./, m => m.toUpperCase())
   .trim();

function SunTime(props) {
  const { name, date, geoPosition, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const times = SunCalc.getTimes(
    date,
    geoPosition.latitude,
    geoPosition.longitude,
    geoPosition.altitude
  );

  const label = splitCamelCase(name);

  if (!(name in times)) {
    return <span className="error">{label} is not available</span>;
  }

  let start, end;

  if (name + "Start" in times) {
    start = times[name + "Start"];
    end = times[name];
  } else {
    start = times[name];
    if (name + "End" in times) {
      end = times[name + "End"];
    }
  }

  const hasChildren = React.Children.count(props.children);

  return (
    <div className="suntime">
      <label>{label}</label>
      {
        end
        ? <>
            <span className="start">{formatTime(start)}</span>
            &nbsp;&mdash;&nbsp;
            <span className="end">{formatTime(end)}</span>
          </>
        : <span className="time">{formatTime(start)}</span>
      }
      {
        hasChildren &&
          <>
            <article style={{ display: isActive ? "block" : "none" }}>
              {props.children}
            </article>
          </>
      }
    </div>
  );
}

SunTime.defaultProps = {
  isActive: false,
};

SunTime.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  name: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  geoPosition: PropTypes.exact({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    altitude: PropTypes.number,
  }).isRequired,
};

export default SunTime;
