import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { earth, perihelion, aphelion } from 'astronomia/src/perihelion';
import { Calendar, JDToDate } from 'astronomia/src/julian';
import Event from '../Event';
import './EarthApsis.css';

function EarthApsis(props) {
  const { date, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const year = new Calendar(date).toYear();

  const perihelionDate = JDToDate(perihelion(earth, year));
  const aphelionDate = JDToDate(aphelion(earth, year));

  return (
    <>
      <div className="earth-apsis">
        <Event name="Perihelion" date={perihelionDate} />
        <Event name="Aphelion" date={aphelionDate} />
        <article style={{ display: isActive ? "block" : "none" }}>
          {props.children}
        </article>
      </div>
    </>
  );
}

EarthApsis.defaultProps = {
  isActive: false,
};

EarthApsis.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
};

export default EarthApsis;
