import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { TYPE, solar } from 'astronomia/src/eclipse';
import { Calendar, JDEToDate } from 'astronomia/src/julian';
import DateTime from '../../display/DateTime';
import './SolarEclipse.css';

function SolarEclipse(props) {
  const { date, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const year = new Calendar(date).toYear();

  const { type, central, jdeMax } = solar(year);

  let eclipseDate;
  if (type !== TYPE.None) {
    eclipseDate = JDEToDate(jdeMax);
  }

  return (
    <>
      <div className="solar-eclipse">
        {
          type === TYPE.None
          ? <p className="none">No solar eclipses</p>
          : (
            <div className="solar">
              <svg className="eclipse" width="48" height="48"
                viewBox="0 0 48 48"
              >
                <mask id="moon">
                  <circle cx="24" cy="24" r="24" fill="white" />
                  {type === TYPE.Total &&
                    <circle cx="24" cy="24" r="24" fill="black" />
                  }
                  {(type === TYPE.Annular || type === TYPE.AnnularTotal) &&
                    <circle cx="24" cy="24" r="22" fill="black" />
                  }
                  {type === TYPE.Partial &&
                    <circle cx="32" cy="32" r="24" fill="black" />
                  }
                </mask>
                <circle cx="24" cy="24" r="23" fill="black" />
                <circle cx="24" cy="24" r="24" fill="yellow"
                  mask="url(#moon)"
                />
              </svg>
              <div className="info">
                <p>
                  {type === TYPE.Total && <b>Total solar eclipse</b>}
                  {type === TYPE.Annular && <b>Annular solar eclipse</b>}
                  {type === TYPE.AnnularTotal && <b>Hybrid solar eclipse</b>}
                  {type === TYPE.Partial && <b>Partial solar eclipse</b>}
                  {central &&
                    <>{" "}<span className="central">Central</span></>
                  }
                </p>
                <p>
                  <DateTime date={eclipseDate} />
                </p>
              </div>
            </div>
          )
        }
        <article style={{ display: isActive ? "block" : "none" }}>
          {type === TYPE.Total &&
            <p>
              A <b>total eclipse</b> occurs when the dark silhouette of the
              Moon completely obscures the intensely bright light of the Sun,
              allowing the much fainter solar corona to be visible.
            </p>
          }
          {type === TYPE.Annular &&
            <p>
              An <b>annular eclipse</b> occurs when the Sun and Moon are
              exactly in line with the Earth, but the apparent size of the Moon
              is smaller than that of the Sun. Hence the Sun appears as a very
              bright ring, or annulus, surrounding the dark disk of the Moon.
            </p>
          }
          {type === TYPE.AnnularTotal &&
            <p>
              A <b>hybrid eclipse</b> (also called <b>annular/total eclipse</b>)
              shifts between a total and annular eclipse. At certain points on
              the surface of Earth, it appears as a total eclipse, whereas at
              other points it appears as annular. Hybrid eclipses are
              comparatively rare.
            </p>
          }
          {type === TYPE.Partial &&
            <p>
              A <b>partial eclipse</b> occurs when the Sun and Moon are not
              exactly in line with the Earth and the Moon only partially
              obscures the Sun.
            </p>
          }
          {central &&
            <p>
              A central eclipse is an eclipse during which the central line of
              the umbra touches the Earth's surface.
            </p>
          }
          {props.children}
        </article>
      </div>
    </>
  );
}

SolarEclipse.defaultProps = {
  isActive: false,
};

SolarEclipse.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
};

export default SolarEclipse;
