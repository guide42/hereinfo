import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import SunCalc from 'suncalc';
import './SunPosition.css';

const altSunrise = 0;
const altZenith = Math.PI / 2;
const altSunset = Math.PI;

const aziSouth = 0;
const aziSouthWest = Math.PI / 4;
const aziWest = Math.PI / 2;
const aziNorthWest = Math.PI / 2 + Math.PI / 4;
const aziNorth = Math.PI;
const aziNorthEast = Math.PI + Math.PI / 4;
const aziEast = Math.PI + Math.PI / 2;
const aziSouthEast = Math.PI + Math.PI / 2 + Math.PI / 4;

const angleSmall = Math.PI / 16;
const angleCloseBefore =
  (altitude, angle) => altitude > angle - angleSmall && altitude < angle;
const angleCloseAfter =
  (altitude, angle) => altitude > angle && altitude < angle + angleSmall;
const angleCloseTo =
  (alt, angle) => angleCloseBefore(alt, angle) || angleCloseAfter(alt, angle);

const formatAngle = angle => `${angle}°`;

const normAngle = angle => angle < 0 ? 2 * Math.PI - Math.abs(angle) : angle;

const timeDec =
  date => date.getHours() + date.getMinutes() / 60 + date.getSeconds() / 3600;
const timeNorm =
  (time, sunrise, sunset) => (time - sunrise) / (sunset - sunrise);

function SunPosition(props) {
  const { date, geoPosition, isActive, onToggle } = props;

  const handleKeyUp = useCallback(
    event => {
      if (isActive && event.key === 'Backspace' && onToggle) {
        onToggle();
      }
    },
    [isActive, onToggle]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp]
  );

  const { altitude, azimuth: azimuth_ } = SunCalc.getPosition(
    date,
    geoPosition.latitude,
    geoPosition.longitude
  );

  let altitudeExplain;
  if (angleCloseTo(altitude, altSunrise)) {
    altitudeExplain = "Sun appearing at the horizon.";
  } else if (angleCloseAfter(altitude, altSunrise)) {
    altitudeExplain = "Sun close to the horizon.";
  } else if (angleCloseBefore(altitude, altZenith)) {
    altitudeExplain = "Sun almost over the head.";
  } else if (angleCloseTo(altitude, altZenith)) {
    altitudeExplain = "Sun over the head.";
  } else if (angleCloseBefore(altitude, altSunset)) {
    altitudeExplain = "Sun almost at the horizon.";
  } else if (angleCloseTo(altitude, altSunset)) {
    altitudeExplain = "Sun disappearing at the horizon.";
  } else if (altitude < 0) {
    altitudeExplain = "Sun under the horizon. It's night.";
  }

  const azimuth = normAngle(azimuth_);

  let position, lookingAt;
  if (geoPosition.latitude) {
    lookingAt = geoPosition.latitude > 0 ? "S" : "N";
  }
  if (angleCloseTo(azimuth, aziSouth)) {
    position = "South";
    lookingAt = "S";
  } else if (angleCloseTo(azimuth, aziSouthWest)) {
    position = "South West";
    lookingAt = "S";
  } else if (angleCloseTo(azimuth, aziWest)) {
    position = "West";
  } else if (angleCloseTo(azimuth, aziNorthWest)) {
    position = "North West";
    lookingAt = "N";
  } else if (angleCloseTo(azimuth, aziNorth)) {
    position = "North";
    lookingAt = "N";
  } else if (angleCloseTo(azimuth, aziNorthEast)) {
    position = "North East";
    lookingAt = "N";
  } else if (angleCloseTo(azimuth, aziEast)) {
    position = "East";
  } else if (angleCloseTo(azimuth, aziSouthEast)) {
    position = "South East";
    lookingAt = "S";
  } else if (azimuth > aziSouth && azimuth < aziSouthWest) {
    position = <>Between <b>S</b> and <b>SW</b></>;
    lookingAt = "S";
  } else if (azimuth > aziSouthWest && azimuth < aziWest) {
    position = <>Between <b>SW</b> and <b>W</b></>;
    lookingAt = "S";
  } else if (azimuth > aziWest && azimuth < aziNorthWest) {
    position = <>Between <b>W</b> and <b>NW</b></>;
    lookingAt = "N";
  } else if (azimuth > aziNorthWest && azimuth < aziNorth) {
    position = <>Between <b>NW</b> and <b>N</b></>;
    lookingAt = "N";
  } else if (azimuth > aziNorth && azimuth < aziNorthEast) {
    position = <>Between <b>N</b> and <b>NE</b></>;
    lookingAt = "N";
  } else if (azimuth > aziNorthEast && azimuth < aziEast) {
    position = <>Between <b>NE</b> and <b>E</b></>;
    lookingAt = "N";
  } else if (azimuth > aziEast && azimuth < aziSouthEast) {
    position = <>Between <b>E</b> and <b>SE</b></>;
    lookingAt = "S";
  } else if (azimuth > aziSouthEast) {
    position = <>Between <b>SE</b> and <b>S</b></>;
    lookingAt = "S";
  } else {
    position = formatAngle(azimuth);
  }

  const { sunrise, sunset } = SunCalc.getTimes(
    date,
    geoPosition.latitude,
    geoPosition.longitude,
    geoPosition.altitude
  );

  const dateTime = timeDec(date);
  const sunriseTime = timeDec(sunrise);
  const sunsetTime = timeDec(sunset);

  let sun;
  if (dateTime > sunriseTime && dateTime < sunsetTime) {
    const time = timeNorm(dateTime, sunriseTime, sunsetTime);
    const timeAngle = Math.PI + Math.PI * time;

    let sunX = 96 + 90 * Math.cos(timeAngle);
    if (lookingAt === "N") {
      sunX = 192 - sunX;
    }

    sun = <circle r="6" fill="orange"
      cx={sunX}
      cy={64 + 52 * Math.sin(timeAngle)}
    />;
  }

  return (
    <div className="sun-position">
      <div className="sun">
        <div className="horizon">
          <svg className="sun-horizon" width="192" height="64" viewBox="0 0 192 64">
            <rect x="0" y="0" width="192" height="64" fill="lightskyblue" />
            <ellipse cx="96" cy="64" rx="90" ry="52" fill="transparent"
              stroke="yellow"
              strokeDasharray="3"
            />
            {sun}
            <rect x="0" y="60" width="192" height="4" fill="forestgreen" />
            <g>
              <ellipse cx="100" cy="52" rx="2" ry="4" fill="black" />
              <line x1="99" y1="48" x2="90" y2="52" stroke="black" />
              <line x1="99" y1="56" x2="90" y2="52" stroke="black" />
              <line x1="102" y1="48" x2="106" y2="46" stroke="black" />
              <line x1="102" y1="56" x2="106" y2="58" stroke="black" />
            </g>
            <text x="2" y="58" style={{fontSize: "70%"}}>
              {lookingAt === "N" ? "W" : "E"}
            </text>
            <text x="178" y="58" style={{fontSize: "70%"}}>
              {lookingAt === "N" ? "E" : "W"}
            </text>
          </svg>
        </div>
        <div className="info">
          <p>
            <label>Sun&nbsp;Pos.</label>
            <span className="position">{position}</span>
          </p>
          {altitudeExplain && <p>{altitudeExplain}</p>}
        </div>
      </div>
      <article style={{ display: isActive ? "block" : "none" }}>
        {geoPosition && geoPosition.latitude > 0 &&
          <p>
            The winter Sun rises in the southeast, transits the celestial
            meridian at a low angle in the south, and then sets in the
            southwest. In summer, the Sun rises in the northeast, peaks out
            slightly south of overhead point (lower in the south at higher
            latitude), and then sets in the northwest.
          </p>
        }
        {geoPosition && geoPosition.latitude < 0 &&
          <p>
            The winter Sun rises in the northeast, peaks out at a low angle in
            the north, and then sets in the northwest. In summer, the Sun rises
            in the southeast, peaks out slightly north of overhead point (lower
            in the north at higher latitude), and then sets in the southwest.
          </p>
        }
        {props.children}
      </article>
    </div>
  );
}

SunPosition.defaultProps = {
  isActive: false,
};

SunPosition.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
  geoPosition: PropTypes.exact({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    altitude: PropTypes.number,
  }).isRequired,
};

export default SunPosition;
