import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { ALARMS } from './alarms';
import * as serviceWorker from './serviceWorker';
import 'typeface-open-sans';
import './index.css';

ReactDOM.render(<App />, document.getElementById('hereinfo'));
serviceWorker.register();

if (navigator.mozSetMessageHandler) {
  navigator.mozSetMessageHandler('alarm', function(mozAlarm) {
    switch (mozAlarm.data.type) {
      case ALARMS.NOTIFY:
        if (Notification && Notification.permission === 'granted') {
          const { title, body } = mozAlarm.data;
          const notification = new Notification(title, { body });
          notification.addEventListener('click', () => {
            notification.close();
          });
        }
        break;
      default:
        break;
    }
  });
}
