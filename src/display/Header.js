import React from 'react';
import './Header.css';

function Header(props) {
  const { children } = props;

  return (
    <h4>{children}</h4>
  );
}

export default Header;
