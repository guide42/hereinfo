import React from 'react';
import PropTypes from 'prop-types';
import { formatTimezone } from '../helpers';

function Timezone(props) {
  const { timezone } = props;

  return (
    <span className="timezone">{formatTimezone(timezone)}</span>
  );
}

Timezone.propTypes = {
  timezone: PropTypes.number.isRequired,
};

export default Timezone;
