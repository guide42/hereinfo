import React from 'react';
import PropTypes from 'prop-types';
import { formatAltitude, formatLatitude, formatLongitude } from '../helpers';

function GeoPosition(props) {
  const { latitude, longitude, altitude } = props;

  return (
    <span className="geo-position">
      <span className="latitude">
        {formatLatitude(latitude)}
      </span>
      ,{" "}
      <span className="longitude">
        {formatLongitude(longitude)}
      </span>
      {
        altitude &&
          <>
            {" "}
            <span className="altitude">{formatAltitude(altitude)}</span>
            &nbsp;
            m.o.s.l.
          </>
      }
    </span>
  );
}

GeoPosition.propTypes = {
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  altitude: PropTypes.number,
};

export default GeoPosition;
