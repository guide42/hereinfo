import React from 'react';
import PropTypes from 'prop-types';
import format from 'date-fns/format';
import { formatDate, formatISO8601 } from '../helpers';

function DateTime(props) {
  const { date, limit } = props;

  const renderDate = () => {
    switch (limit) {
      case 'date': return format(date, 'PPP');
      case 'time': return format(date, 'p');
      default: return formatDate(date);
    }
  };

  return (
    <time dateTime={formatISO8601(date)} className="datetime">
      {renderDate()}
    </time>
  );
}

DateTime.propTypes = {
  date: PropTypes.instanceOf(Date).isRequired,
  limit: PropTypes.string,
};

export default DateTime;
