import { earth, perihelion, aphelion } from 'astronomia/src/perihelion';
import { perigee, apogee } from 'astronomia/src/apsis';
import { ascending, descending } from 'astronomia/src/moonnode';
import { solar, lunar, TYPE } from 'astronomia/src/eclipse';
import { newMoon, first, full, last } from 'astronomia/src/moonphase';
import { north, south } from 'astronomia/src/moonmaxdec';
import { march, june, september, december } from 'astronomia/src/solstice';
import { Calendar } from 'astronomia/src/julian';
import compareAsc from 'date-fns/compareAsc';

/**
 * List of events for the CalendarEvents component. Each event is composed of
 * a name, a function that calculates the time and should return a Calendar
 * instance or an object with Julian day or the Julian ephemeris day; and a
 * function that returns the plain text to be displayed.
 */

export const EVENTS = [
  [
    'Perihelion',
    (year) => {
      return {
        jde: perihelion(earth, year),
      };
    },
    () => "Closest distance to the Sun.",
  ],
  [
    'Aphelion',
    (year) => {
      return {
        jde: aphelion(earth, year),
      };
    },
    () => "Farthest distance to the Sun.",
  ],
  [
    'Moon Perigee',
    (year) => {
      return {
        jde: perigee(year),
      };
    },
    () => "Closest distance between the Moon and Earth.",
  ],
  [
    'Moon Apogee',
    (year) => {
      return {
        jde: apogee(year),
      };
    },
    () => "Farthest distance between the Moon and Earth.",
  ],
  [
    'Moon Ascending',
    (year) => {
      return {
        jde: ascending(year),
      };
    },
    () => "Entering the Northern Celestial Hemisphere.",
  ],
  [
    'Moon Descending',
    (year) => {
      return {
        jde: descending(year),
      };
    },
    () => "Moving into the Southern Celestial Hemisphere.",
  ],
  [
    'Solar Eclipse',
    (year) => solar(year),
    (data) => {
      const central = data.central ? " Central." : "";
      switch (data.type) {
        case TYPE.Total:
          return "Total solar eclipse." + central;
        case TYPE.Annular:
          return "Annular solar eclipse." + central;
        case TYPE.AnnularTotal:
          return "Hybrid solar eclipse." + central;
        case TYPE.Partial:
          return "Partial solar eclipse." + central;
        default:
          return "Solar eclipse." + central;
      }
    },
  ],
  [
    'Lunar Eclipse',
    (year) => lunar(year),
    (data) => {
      switch (data.type) {
        case TYPE.Total:
          return "Total lunar eclipse.";
        case TYPE.Penumbral:
          return "Penumbral lunar eclipse.";
        case TYPE.Umbral:
          return "Umbral lunar eclipse.";
        default:
          return "Lunar eclipse.";
      }
    },
  ],
  [
    'New Moon',
    (year) => {
      return {
        jde: newMoon(year),
      };
    },
    () => "Moon disc completely in Sun's shadow (lit by earthshine only).",
  ],
  [
    'First Quarter Moon',
    (year) => {
      return {
        jde: first(year),
      };
    },
    () => "Moon is 50% lit.",
  ],
  [
    'Full Moon',
    (year) => {
      return {
        jde: full(year),
      };
    },
    () => "Moon is 100% illuminated.",
  ],
  [
    'Last Quarter Moon',
    (year) => {
      return {
        jde: last(year),
      };
    },
    () => "Moon is 50% lit.",
  ],
  [
    'Moon Northern Decl.',
    (year) => north(year),
    () => "Moon is most northerly.",
  ],
  [
    'Moon Southern Decl.',
    (year) => south(year),
    () => "Moon is most southerly.",
  ],
  [
    'March Equinox',
    (year) => {
      return {
        jde: march(Math.trunc(year)),
      };
    },
    () => "Sun appears to cross the celestial equator northward.",
  ],
  [
    'June Solstice',
    (year) => {
      return {
        jde: june(Math.trunc(year)),
      };
    },
    () => "Sun appears to reach its most far excursion relative to the equator.",
  ],
  [
    'September Equinox',
    (year) => {
      return {
        jde: september(Math.trunc(year)),
      };
    },
    () => "Sun appears to cross the celestial equator southward.",
  ],
  [
    'December Solstice',
    (year) => {
      return {
        jde: december(Math.trunc(year)),
      };
    },
    () => "Sun appears to reach its most far excursion relative to the equator.",
  ],
];

export function calculateEvents(year) {
  const calculated = [];
  EVENTS.forEach(([name, calc, render]) => {
    let cal;
    let data;
    const ret = calc(year);
    if (typeof ret === 'object') {
      if ('jd' in ret || 'jdMax' in ret) {
        cal = new Calendar().fromJD(ret.jd || ret.jdMax);
      } else if ('jde' in ret || 'jdeMax' in ret) {
        cal = new Calendar().fromJDE(ret.jde || ret.jdeMax);
      }
      data = ret;
    } else {
      cal = ret;
      data = {};
    }
    if (cal instanceof Calendar) {
      calculated.push([name, cal.toDate(), render, data]);
    }
  });
  return calculated.sort((left, right) => {
    return compareAsc(left[1], right[1]);
  });
}
