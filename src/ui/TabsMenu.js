import React, { useEffect, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import telescope from '../telescope.svg';
import './TabsMenu.css';

function TabsMenu(props) {
  const onMenuChange = props.onMenuChange;
  const active = props.active;
  const navRef = useRef();
  const scrollTo = useCallback(
    (index) => {
      navRef.current.firstElementChild.childNodes[index].scrollIntoView({
        behavior: 'auto',
        block: 'start',
        inline: 'center',
      });
    },
    [navRef]
  );
  const handleTabChange = useCallback(
    (index) => {
      onMenuChange(index);
    },
    [onMenuChange]
  );
  const handleOnClick = useCallback(
    (e, index) => {
      handleTabChange(index);
    },
    [handleTabChange]
  );
  const renderNav = () => props.labels.map(
    (label, index) => (
      <li
        key={label}
        onClick={e => handleOnClick(e, index)}
        className={active === index ? "active" : undefined}
      >
        <span>
          {label === 'Here INFO'
            ? <img src={telescope} alt={label} height="12" />
            : label}
        </span>
      </li>
    )
  );
  const labelsCount = props.labels.length;
  const handleKeyUp = useCallback(
    event => {
      if (event.target !== document.body) {
        return;
      }
      let index;
      switch (event.key) {
        case 'ArrowRight':
          index = Math.min(active + 1, labelsCount - 1);
          if (active !== index) {
            handleTabChange(index);
          }
          break;
        case 'ArrowLeft':
          index = Math.max(active - 1, 0);
          if (active !== index) {
            handleTabChange(index);
          }
          break;
        default:
          break;
      }
    },
    [active, labelsCount, handleTabChange]
  );
  useEffect(
    () => {
      setTimeout(() => scrollTo(active), 40);
      document.addEventListener('keyup', handleKeyUp);
      return () => document.removeEventListener('keyup', handleKeyUp);
    },
    [handleKeyUp, scrollTo, active]
  );
  return (
    <nav ref={navRef}>
      <ol>
        {renderNav()}
      </ol>
    </nav>
  );
}

TabsMenu.propTypes = {
  active: PropTypes.number.isRequired,
  labels: PropTypes.arrayOf(PropTypes.string).isRequired,
  onMenuChange: PropTypes.func.isRequired,
};

export default TabsMenu;
