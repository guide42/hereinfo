import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Calendar } from 'astronomia/src/julian';
import format from 'date-fns/format';
import addWeeks from 'date-fns/addWeeks';
import isSameDay from 'date-fns/isSameDay';
import isSameWeek from 'date-fns/isSameWeek';
import isThisWeek from 'date-fns/isThisWeek';
import isToday from 'date-fns/isToday';
import isYesterday from 'date-fns/isYesterday';
import isPast from 'date-fns/isPast';
import { calculateEvents } from '../events';
import ListSection from '../ui/ListSection';
import AlertEvent from '../view/AlertEvent';
import Header from '../display/Header';
import Loading from '../display/Loading';
import './CalendarEvents.css';

function CalendarEvents(props) {
  const isActive = props.isActive;
  const [data, setData] = useState(null);
  const [dataYear, setDataYear] = useState(null);
  const year = new Calendar(props.date).toYear();

  useEffect(
    () => {
      if (data === null || dataYear !== year) {
        setDataYear(year);
        setData(calculateEvents(year));
      }
    },
    [year, data, dataYear, setData, setDataYear]
  );

  if (data === null) {
    return (
      <section aria-hidden={!props.isActive}>
        <Loading />
      </section>
    );
  }

  const renderChildren = () => {
    let children = [];
    let lastDate;
    data.forEach(([name, date, render, data]) => {
      if (!isSameDay(lastDate, date)) {
        const header = (() => {
          switch (true) {
            case isToday(date):
              return "Today";
            case isYesterday(date):
              return "Yesterday";
            case isThisWeek(date):
              if (isPast(date)) {
                return "Last " + format(date, 'cccc');
              } else {
                return format(date, 'cccc');
              }
            case isSameWeek(date, addWeeks(new Date(), 1)):
              return "Next " + format(date, 'cccc');
            default:
              return format(date, 'PPPP');
          }
        })();
        children.push(
          <Header key={header} disabled={true}>{header}</Header>
        );
      }
      const text = render(data);
      children.push(
        <AlertEvent key={name} name={name} date={date} limit="time" text={text}>
          <p>{text}</p>
        </AlertEvent>
      );
      lastDate = date;
    });
    return children;
  };

  return (
    <ListSection onFocus={props.onFocus} isActive={isActive}>
      {renderChildren()}
    </ListSection>
  );
}

CalendarEvents.defaultProps = {
  isActive: false,
};

CalendarEvents.propTypes = {
  isActive: PropTypes.bool,
  onFocus: PropTypes.func,
  date: PropTypes.instanceOf(Date).isRequired,
};

export default CalendarEvents;
