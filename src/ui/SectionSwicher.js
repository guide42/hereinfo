import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import './SectionSwicher.css';

function SectionSwicher(props) {
  const divRef = useRef();

  const active = props.active;

  const renderChildren = () => React.Children.map(props.children,
    (child, index) => React.cloneElement(child, {
      isActive: active === index,
    })
  );

  const percentage = active * 100 * -1;

  useEffect(
    () => {
      setTimeout(
        () => divRef.current.style.transform = `translate(${percentage}%, 0)`,
        40
      );
    },
    [
      divRef,
      percentage,
    ]
  );

  return (
    <div
      className="sections"
      ref={divRef}
    >
      {renderChildren()}
    </div>
  );
}

SectionSwicher.propTypes = {
  active: PropTypes.number.isRequired,
};

export default SectionSwicher;
