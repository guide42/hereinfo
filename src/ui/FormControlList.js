import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import './FormControlList.css';

function FormControlList(props) {
  const isActive = props.isActive;
  const onToggle = props.onToggle;
  const formRef = props.formRef;
  const [focused, setFocused] = useState(-1);

  const focusIndex = useCallback(
    index => {
      formRef.current.elements[index].focus();
    },
    [formRef]
  );

  const blurAll = useCallback(
    () => {
      for (let i = 0; i < formRef.current.elements.length; i++) {
        formRef.current.elements[i].blur();
      }
    },
    [formRef]
  );

  const handleOnFocus = (index, event) => {
    let i;
    for (i = 0; i < formRef.current.elements.length; i++) {
      if (formRef.current.elements[i] === event.target) {
        break;
      }
    }
    if (i === formRef.current.elements.length) {
      setFocused(index);
    } else {
      setFocused(i);
    }
  };

  const handleOnBlur = (index, event) => {
    setFocused(-1);
  };

  const handleKeyDown = useCallback(
    event => {
      if (
        isActive &&
        event.key === 'Enter' &&
        event.target.tagName !== 'BUTTON' &&
        focused >= 0 &&
        focused < formRef.current.elements.length
      ) {
        event.preventDefault();
      }
    },
    [isActive, focused, formRef]
  );

  const handleKeyUp = useCallback(
    event => {
      if (!isActive) {
        return;
      }
      switch (event.key) {
        case 'Call':
          blurAll();
          setFocused(-1);
          break;
        case 'Backspace':
          if (
            focused === -1 ||
            focused === formRef.current.elements.length ||
            event.target.nodeName === 'BUTTON'
          ) {
            onToggle && onToggle();
          }
          break;
        case 'SoftLeft': case 'l':
          if (focused - 1 < 0) {
            blurAll();
            setFocused(-1);
          } else {
            focusIndex(focused - 1);
            setFocused(focused - 1);
          }
          break;
        case 'SoftRight': case 'r':
          if (focused + 1 > formRef.current.elements.length - 1) {
            blurAll();
            setFocused(formRef.current.elements.length);
          } else {
            focusIndex(focused + 1);
            setFocused(focused + 1);
          }
          break;
        default:
          break;
      }
    },
    [isActive, onToggle, focused, setFocused, formRef, focusIndex, blurAll]
  );

  useEffect(
    () => {
      document.addEventListener('keyup', handleKeyUp);
      document.addEventListener('keydown', handleKeyDown);
      return () => {
        document.removeEventListener('keyup', handleKeyUp);
        document.removeEventListener('keydown', handleKeyDown);
      };
    },
    [handleKeyUp, handleKeyDown]
  );

  const renderChildren = () => React.Children.map(
    props.children,
    (child, index) => (
      <div key={index} className="control">
        {React.cloneElement(child, {
          onFocus: event => handleOnFocus(index, event),
          onBlur: event => handleOnBlur(index, event),
        })}
      </div>
    )
  );

  return (
    <>
      {renderChildren()}
    </>
  );
}

FormControlList.defaultProps = {
  isActive: false,
};

FormControlList.propTypes = {
  isActive: PropTypes.bool,
  onToggle: PropTypes.func,
  formRef: PropTypes.object.isRequired,
};

export default FormControlList;
