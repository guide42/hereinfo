import React from 'react';
import PropTypes from 'prop-types';
import './ListSection.css';

class ListSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {focus: -1, active: -1};
    this.handleToggle = this.handleToggle.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.sectionRef = React.createRef();
    this.childrenRefs = [];
    for (let i = 0; i < React.Children.count(props.children); i++) {
      this.childrenRefs[i] = React.createRef();
    }
  }
  handleToggle() {
    if (this.state.active === -1 && this.state.focus >= 0) {
      this.setActive(this.state.focus);
    } else {
      this.setState({active: -1});
    }
  }
  handleKeyDown(event) {
    if (!this.props.isActive || event.target !== document.body) {
      return;
    }
    let index = this.state.focus;
    switch (event.key) {
      case 'Backspace':
        if (this.state.active !== -1) {
          event.preventDefault();
        }
        break;
      case 'Enter':
        if (index >= 0) {
          this.setActive(index);
        }
        break;
      case 'ArrowUp':
        if (this.state.active === -1) {
          do {
            index = index - 1 >= 0
              ? index - 1
              : React.Children.count(this.props.children) - 1;
          } while (this.isDisabled(index));
          event.preventDefault();
          this.setFocus(index);
        } else {
          this.sectionRef.current.scrollBy({
            top: -100,
            behavior: 'smooth',
          });
        }
        break;
      case 'ArrowDown':
        if (this.state.active === -1) {
          do {
            index = index + 1 < React.Children.count(this.props.children)
              ? index + 1
              : 0;
          } while (this.isDisabled(index));
          event.preventDefault();
          this.setFocus(index);
        } else {
          this.sectionRef.current.scrollBy({
            top: 100,
            behavior: 'smooth',
          });
        }
        break;
      default:
        break;
    }
  }
  handleDoubleClick(index, event) {
    if (this.isDisabled(index)) {
      return;
    }
    if (this.state.focus !== index) {
      this.setFocus(index);
    } else if (this.state.active !== index) {
      this.setActive(index);
    } else {
      this.setState({active: -1});
    }
  }
  handleClick(index, event) {
    if (this.isDisabled(index)) {
      return;
    }
    if (this.state.focus !== index) {
      this.setFocus(index);
    }
  }
  isDisabled(index) {
    if (!Array.isArray(this.props.children) && index === 0) {
      return this.props.children.props.disabled === true;
    }
    return this.props.children[index].props.disabled === true;
  }
  setFocus(index) {
    this.setState({focus: index, active: -1});
    if ('onFocus' in this.props) {
      this.props.onFocus(index);
    }
    setTimeout(
      () => {
        this.childrenRefs[index].current.scrollIntoView(false)
      },
      50
    );
  }
  setActive(index) {
    this.setState({active: index});
    setTimeout(
      () => {
        this.childrenRefs[index].current.scrollIntoView(true)
      },
      50
    );
  }
  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown);
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }
  componentDidUpdate(prevProps, prevState) {
    if (!this.props.isActive && this.state.active >= 0) {
      this.setState({active: -1});
    }
  }
  renderChildren() {
    return React.Children.map(this.props.children, (child, index) => (
      <li
        ref={this.childrenRefs[index]}
        key={index}
        className={
          (this.state.focus === index ? "focused" : "unfocused") + " " +
          (this.state.active === index ? "activated" : "deactivated") + " " +
          (child.props.disabled === true ? "disabled" : "enabled")
        }
        onClick={e => this.handleClick(index, e)}
        onDoubleClick={e => this.handleDoubleClick(index, e)}
      >
        {React.cloneElement(child, {
          isActive: this.state.active === index,
          onToggle: this.handleToggle,
        })}
      </li>
    ));
  }
  render() {
    return (
      <section ref={this.sectionRef} aria-hidden={!this.props.isActive}>
        <ul>
          {this.renderChildren()}
        </ul>
      </section>
    );
  }
}

ListSection.defaultProps = {
  isActive: false,
};

ListSection.propTypes = {
  isActive: PropTypes.bool,
  onFocus: PropTypes.func,
};

export default ListSection;
