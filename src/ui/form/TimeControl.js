import React from 'react';
import PropTypes from 'prop-types';
import './TimeControl.css';

function TimeControl(props) {
  const { hour, minute, onHourChange, onMinuteChange, onFocus, onBlur } = props;
  return (
    <>
      <input
        className="hour"
        type="number"
        value={hour}
        onChange={onHourChange}
        onFocus={onFocus}
        onBlur={onBlur}
      />
      :
      <input
        className="minute"
        type="number"
        value={minute}
        onChange={onMinuteChange}
        onFocus={onFocus}
        onBlur={onBlur}
      />
    </>
  );
}

TimeControl.propTypes = {
  hour: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  minute: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onHourChange: PropTypes.func,
  onMinuteChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
};

export default TimeControl;
