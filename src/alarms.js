const NOTIFY = 'notify';

export function notify(title, body) {
  return {type: NOTIFY, title, body};
}

export const ALARMS = {
  NOTIFY,
};
