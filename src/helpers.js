export const pad = n => n < 10 ? '0' + n : n;

export function tzOffsetToTime(offset) {
  const sym = offset < 0 ? "+" : "-";
  const tz = Math.abs(offset / 60);
  const tzHour = pad(Math.trunc(tz));
  const tzMinutes = pad((tz - Math.trunc(tz)) * 60);
  return `${sym}${tzHour}:${tzMinutes}`;
}

//export function tzTimeToOffset(time) {
//  const sym = time.substr(0, 1);
//  const tzHour = parseInt(time.substr(1, 3));
//  const tzMinutes = parseInt(time.substr(5, 7));
//  return tzHour + tzMinutes / 60 * (sym === "+" ? -1 : 1);
//}

const dateTimeFormatter = new Intl.DateTimeFormat(undefined, {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
});

export const formatDate = (date) => dateTimeFormatter.format(date);

export const formatTimezone = (timezone) => `UTC${tzOffsetToTime(timezone)}`;

export const formatISO8601 =
  (date) => date.getFullYear() +
    '-' + pad(date.getMonth() + 1) +
    '-' + pad(date.getDate());


export const formatAltitude = (altitude) => altitude.toFixed(2);

function formatSexagesimal(coord) {
  const degrees = Math.trunc(coord);
  const minutes = Math.trunc(60 * (coord - degrees));
  const seconds = 3600 * (coord - degrees) - 60 * minutes;
  return `${Math.abs(degrees)}° ${Math.abs(minutes)}′ ${Math.abs(seconds.toFixed(2))}″`;
}

export const formatLatitude =
  (latitude) => `${formatSexagesimal(latitude)} ${latitude > 0 ? "N" : "S"}`;

export const formatLongitude =
  (longitude) => `${formatSexagesimal(longitude)} ${longitude > 0 ? "E" : "W"}`;

const numberFormat = Intl.NumberFormat();

export const formatDistance = d => `${numberFormat.format(d)} km`;
