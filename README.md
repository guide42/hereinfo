# Here INFO

About where you are here and now, rise and set hours, astronomical positions,
moon age, and much more.

Aims to be a knowledge base for the amateur astronomer and general curious
person using KaiOS/GerdaOS phone.

## Screenshots

<img src="docs/about.png" /> <img src="docs/today.png" /> <img src="docs/calendar.png" /> <img src="docs/moon.png" /> <img src="docs/sun.png" />

## Contributing

Yes, please! Check [`CONTRIBUTING.md`](CONTRIBUTING.md) for guidelines.

## License

Code of this KaiOS/GerdaOS application is under ISC license.

Thanks Wikipedia for some of the texts (CC Attribution-ShareAlike.)

Thanks GitHub Inc for the telescope icon (MIT License.)

Thanks Banana Hackers.
